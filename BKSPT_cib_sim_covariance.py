'''
This is used to generate CIB alms for the covariance case where we use as input spectra CIB auto and cross spectra drawn from the statistical distribution of 10 foreground clean patches:

http://bicep.rc.fas.harvard.edu/bkspt/analysis_logbook/analysis/20171024_test_patches/
'''

from scipy.interpolate import InterpolatedUnivariateSpline
# import sys
import os
import healpy as hp
import numpy as np
from joblib import Parallel, delayed
from scipy.signal import savgol_filter


def bin_spectra(spectrum, li, average_window=81):
    '''

    '''
    spectrum_filtered = savgol_filter(spectrum, average_window, 0)
    #     spectrum_filtered=spectrum
    cl_spline_mean = InterpolatedUnivariateSpline(
        np.arange(0, len(spectrum_filtered)), spectrum_filtered)
    cl_li = cl_spline_mean(li)

    return cl_li


def simulate_parallel_cov_patches(idx, cc, cp, cc_filter, cp_filter, pp, li,
                                  lmin, lmax, phi_folder, cib_output_folder):
    '''
    This given a fiducial cc and cp for the filter and a cc cp that will be used for sims generate n_sims CIB skies given the lensing potential sky
    '''
    assert (np.all(np.greater_equal(cp, 0)))

    print('in parallel patches')
    spl_cppp = InterpolatedUnivariateSpline(li, np.nan_to_num(cp / pp / li**2))

    spl = InterpolatedUnivariateSpline(li, np.nan_to_num(cc - cp**2 / pp))
    # we ALWAYS filter with the reference mean value
    filter_ell = InterpolatedUnivariateSpline(
        li, np.nan_to_num(cp_filter / cc_filter * li**2))
    # np.save('./filter_ell', np.vstack((li, filter_ell(li))).T)

    # for partial reproducibility
    np.random.seed(idx + 600)
    # np.random.seed(idx)

    print('generating CIB skies from data covariances n=', idx)
    phi_alm = hp.read_alm(phi_folder + '/phi_alms_r{0:04d}.fits'.format(idx)
                          ).astype('complex128')

    high_ell_filter = np.ones(hp.Alm.getlmax(len(phi_alm)))
    high_ell_filter[lmax:-1] = 0.

    low_ell_filter = np.ones(hp.Alm.getlmax(len(phi_alm)))
    low_ell_filter[:lmin] = 0.

    # print('phi ell max', hp.Alm.getlmax(len(phi_alm)), lmax)
    phi_alm = hp.almxfl(phi_alm, high_ell_filter, inplace=True)
    phi_alm = hp.almxfl(phi_alm, low_ell_filter, inplace=True)
    # print(np.min(spl(np.arange(0, lmax))))
    phi_cross_cl = spl_cppp(np.arange(0, lmax))
    phi_cross_cl[np.where(phi_cross_cl < 0)] = 0.
    assert (np.all(np.greater_equal(phi_cross_cl, 0)))

    phi_cross = hp.almxfl(
        phi_alm, phi_cross_cl * np.arange(0, lmax)**2, inplace=True)

    del phi_alm

    # compute cib uncorrelated variance
    # add cib noise variance
    noise_cl = spl(np.arange(0, lmax))
    noise_cl[np.where(noise_cl < 0)] = 0
    assert (np.all(np.greater_equal(noise_cl, 0)))

    noise = hp.synalm(noise_cl, lmax=hp.Alm.getlmax(len(phi_cross)), new=True)

    noise = hp.almxfl(noise, high_ell_filter, inplace=True)
    noise = hp.almxfl(noise, low_ell_filter, inplace=True)

    assert (np.all(np.greater_equal(filter_ell(np.arange(1, lmax)), 0)))

    WN = hp.almxfl(
        noise,
        np.concatenate(
            ([0], filter_ell(np.arange(1, lmax)) / np.arange(1, lmax)**2)))
    WS = hp.almxfl(
        phi_cross,
        np.concatenate(
            ([0], filter_ell(np.arange(1, lmax)) / np.arange(1, lmax)**2)))

    filtered_cib = WN + WS
    unfiltered_cib = noise + phi_cross

    if not os.path.exists(cib_output_folder + '/patches_joint_seed1_2/'):
        os.makedirs(cib_output_folder + '/patches_joint_seed1_2/')

    overwrite = False
    hp.write_alm(
        cib_output_folder + '/patches_joint_seed1_2/' +
        'WSN_alms_r{:04d}.fits'.format(idx),
        filtered_cib,
        overwrite=overwrite)
    hp.write_alm(
        cib_output_folder + '/patches_joint_seed1_2/' +
        'WN_alms_r{:04d}.fits'.format(idx),
        WN,
        overwrite=overwrite)
    hp.write_alm(
        cib_output_folder + '/patches_joint_seed1_2/' +
        'WS_alms_r{:04d}.fits'.format(idx),
        WS,
        overwrite=overwrite)
    hp.write_alm(
        cib_output_folder + '/patches_joint_seed1_2/' +
        'S_alms_r{:04d}.fits'.format(idx),
        phi_cross,
        overwrite=overwrite)
    hp.write_alm(
        cib_output_folder + '/patches_joint_seed1_2/' +
        'SN_alms_r{:04d}.fits'.format(idx),
        unfiltered_cib,
        overwrite=overwrite)


def load_samples_spectra(cib_spectra_input_folder='./data_cib_sims'):
    '''
    This load or generate the 500 CIB and CIB-phi spectra generated according to the given cov matrix and mean
    '''

    # if file with spectra already generated does not exists create them and save.
    if os.path.isfile(
            cib_spectra_input_folder + '/cross_auto_spectra_seed1.npy'):

        sim_spectra = np.load(
            cib_spectra_input_folder + '/cross_auto_spectra_seed1.npy')
        cross_spectra = sim_spectra[:, 1501:]
        auto_spectra = sim_spectra[:, :1501]

    else:
        print("generating a new set of joint spectra")
        cov_spectra = np.load(
            cib_spectra_input_folder + '/10patches_171215_joint_cov.npy')
        mean_spectra = np.load(
            cib_spectra_input_folder + '/10patches_171215_joint_mean.npy')
        #
        # np.random.seed(51)
        np.random.seed(100)

        sim_spectra = np.random.multivariate_normal(
            mean_spectra, cov_spectra, size=500)

        np.save(cib_spectra_input_folder + '/cross_auto_spectra_seed1',
                sim_spectra)
        cross_spectra = sim_spectra[:, 1501:]
        auto_spectra = sim_spectra[:, :1501]

    return cross_spectra, auto_spectra


if __name__ == '__main__':

    n_sims = 20
    n_cpus = 1
    # here is where sims live
    folder = '/n/holylfs02/LABS/kovac_lab/general/input_maps/lensing_potentials/camb_planck2013_r0/'

    # Load the lensing potential power spectrum used to generate the lensig potential skies
    data = np.loadtxt(
        '/n/holylfs02/LABS/kovac_lab/general/input_maps/official_cl/camb_planck2013_extended_r0_lenspotentialcls.dat'
    )

    li = 1. * np.arange(4, 2000)

    pp_spline = InterpolatedUnivariateSpline(data[:, 0],
                                             data[:, 5] / data[:, 0]**2 /
                                             (data[:, 0] + 1.)**2 * 2. * np.pi)
    pp = pp_spline(li)
    # this is to load using splines
    splines = True
    lmax = 1500

    cross_spectra, auto_spectra = load_samples_spectra(
        cib_spectra_input_folder='./data_cib_sims')

    print(
        'among the 500 sims, only this multiple ells have a case with C_ell<0',
        np.unique(np.argwhere(auto_spectra < 0)[:, 1]))

    print(
        'among the 500 sims, only this multiple ells have a case with C_ell<0',
        np.unique(np.argwhere(cross_spectra < 0)[:, 1]))

    list_negative = (np.argwhere(cross_spectra < 0)[:, 1])

    # # we can also do this on the binned spectra. Might be better
    # cross_spectra[np.where(cross_spectra<0)]=0.

    cc_list = Parallel(n_jobs=n_cpus)(
        delayed(bin_spectra)(spectrum=auto_spectra[i], li=li)
        for i in range(0, n_sims))
    cp_list = Parallel(n_jobs=n_cpus)(
        delayed(bin_spectra)(spectrum=cross_spectra[i], li=li)
        for i in range(0, n_sims))
    cp_list = np.array(cp_list) * 2. / (li + 1.) / li

    cp_list[np.where(cp_list < 0)] = 0

    print(
        'WARNING I am setting some ells in the cross correlation to zero because they are actually negatives'
    )
    assert (len(auto_spectra[0]) == len(cross_spectra[0]))
    # print(cp_list)
    mean = np.load('./data_cib_sims/10patches_171215_joint_mean.npy')

    cross_mean = mean[1501:]
    auto_mean = mean[:1501]

    cc_mean = bin_spectra(auto_mean, li)
    cp_mean = np.array(bin_spectra(cross_mean, li)) * 2. / (li + 1.) / li
    cp_mean[np.where(cp_mean < 0)] = 0
    print(cp_mean)

    # Parallel(n_jobs=n_cpus)(delayed(simulate_parallel_cov_patches)(idx=i, cc=cc_list[i], cp=cp_list[i], cc_filter=cc_mean, cp_filter=cp_mean, pp=pp, li=li, lmin=20, lmax=1400) for i in range(1, n_sims))

    Parallel(n_jobs=n_cpus)(delayed(simulate_parallel_cov_patches)(
        idx=i,
        cc=cc_list[i],
        cp=cp_list[i],
        cc_filter=cc_mean,
        cp_filter=cp_mean,
        pp=pp,
        li=li,
        lmin=20,
        lmax=1400,
        phi_folder=folder,
        cib_output_folder='./cib_sims/') for i in range(1, n_sims))
