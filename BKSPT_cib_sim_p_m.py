'''
This is used to generate CIB alms for the plus and minus case where we use as input spectra the mean and mean +-std of the spectra on 10 clean patches described here:

http://bicep.rc.fas.harvard.edu/bkspt/analysis_logbook/analysis/20171024_test_patches/

'''
from scipy.interpolate import InterpolatedUnivariateSpline, UnivariateSpline
# import matplotlib
# import matplotlib.pyplot as plt
# import matplotlib.gridspec as gridspec
# import pylab
# import sys
import os
import healpy as hp
import numpy as np
# import scipy.stats as stat
# import camb
# from camb import model, initialpower
# import imp
# import os
# import quickspec as qs
# import pickle as pk
# import statsmodels.api as sm
from joblib import Parallel, delayed
# import sptpol_software.lensing as sl
# from hashlib import sha1
import sys


def load_spectra_10_patches(li, smoothing_factor=5e-15, fact=0.5):
    '''
    Will load the +- and reference value of the CIB auto and cross with lensing on that patch. These are computed as the mean and variance of 10 cleanest patches
    '''

    # get the mean case, you need it for filtering
    case = 'mean'
    cross_data = np.load(
        './data_cib_sims/cl_cross_10patches_171215_{}.npy'.format(case))[1:]

    std_cib = np.load('./data_cib_sims/cl_cib_10patches_171215_std.npy')[1:]

    std_cross = np.load('./data_cib_sims/cl_cross_10patches_171215_std.npy')[
        1:]

    cib_data = np.load(
        './data_cib_sims/cl_cib_10patches_171215_{}.npy'.format(case))[1:]

    cl_cib_cross_spline_mean = UnivariateSpline(
        np.arange(1,
                  len(cross_data) + 1), cross_data, s=smoothing_factor)

    cl_cib_spline_mean = UnivariateSpline(
        np.arange(0, len(cib_data)), cib_data, s=smoothing_factor)

    cc_mean = cl_cib_spline_mean(li)
    cp_mean = cl_cib_cross_spline_mean(li) * 2. / (li + 1.) / li

    case = 'plus'

    lmax = len(data)

    cl_cib_cross_spline_plus = UnivariateSpline(
        np.arange(1,
                  len(cross_data) + 1),
        cross_data + fact * std_cross,
        s=smoothing_factor)

    cl_cib_spline_plus = UnivariateSpline(
        np.arange(0, len(cib_data)),
        cib_data + fact * std_cib,
        s=smoothing_factor)

    cc_plus = cl_cib_spline_plus(li)
    cp_plus = cl_cib_cross_spline_plus(li) * 2. / (li + 1.) / li

    case = 'minus'

    cl_cib_cross_spline_minus = UnivariateSpline(
        np.arange(1,
                  len(cross_data) + 1),
        cross_data - fact * std_cross,
        s=smoothing_factor)

    cl_cib_spline_minus = UnivariateSpline(
        np.arange(0, len(cib_data)),
        cib_data - fact * std_cib,
        s=smoothing_factor)

    cc_minus = cl_cib_spline_minus(li)
    cp_minus = cl_cib_cross_spline_minus(li) * 2. / (li + 1.) / li

    return cc_mean, cc_plus, cc_minus, cp_mean, cp_plus, cp_minus, lmax


def simulate_parallel_patches(idx, cc, cp, cc_filter, cp_filter, pp, li, case,
                              lmin, lmax, fact, folder_sim_output, folder_phi):
    '''
    This given a fiducial cc and cp for the filter and a cc cp that will be used for sims generate n_sims CIB skies given the lensing potential sky
    '''

    if not os.path.exists(folder_sim_output):
        os.makedirs(folder_sim_output)
    # print('in parallel patches')
    spl_cppp = InterpolatedUnivariateSpline(li, np.nan_to_num(cp / pp / li**2))
    spl = InterpolatedUnivariateSpline(li, np.nan_to_num(cc - cp**2 / pp))
    # we ALWAYS filter with the reference mean value
    filter_ell = InterpolatedUnivariateSpline(
        li, np.nan_to_num(cp_filter / cc_filter * li**2))
    # np.save('./filter_ell', np.vstack((li, filter_ell(li))).T)

    # for partial reproducibility
    np.random.seed(abs(hash(case)) % (10**5) + idx)

    print('generating CIB skies n=', idx, 'for case', case)
    phi_alm = hp.read_alm(folder_phi + '/phi_alms_r{0:04d}.fits'.format(idx)
                          ).astype('complex128')

    high_ell_filter = np.ones(hp.Alm.getlmax(len(phi_alm)))
    high_ell_filter[lmax:-1] = 0.

    low_ell_filter = np.ones(hp.Alm.getlmax(len(phi_alm)))
    low_ell_filter[:lmin] = 0.

    print('phi ell max', hp.Alm.getlmax(len(phi_alm)), lmax)
    phi_alm = hp.almxfl(phi_alm, high_ell_filter, inplace=True)
    phi_alm = hp.almxfl(phi_alm, low_ell_filter, inplace=True)

    phi_cross = hp.almxfl(
        phi_alm,
        spl_cppp(np.arange(0, lmax)) * np.arange(0, lmax)**2,
        inplace=True)

    del phi_alm

    # compute cib uncorrelated variance
    # add cib noise variance
    # assert(np.all(np.greater_equal(spl(np.arange(0, lmax)), 0)))
    print(lmax, lmin)

    noise = hp.synalm(
        spl(np.arange(0, lmax)), lmax=hp.Alm.getlmax(len(phi_cross)), new=True)
    noise = hp.almxfl(noise, high_ell_filter, inplace=True)
    noise = hp.almxfl(noise, low_ell_filter, inplace=True)

    WN = hp.almxfl(
        noise,
        np.concatenate(
            ([0], filter_ell(np.arange(1, lmax)) / np.arange(1, lmax)**2)))
    WS = hp.almxfl(
        phi_cross,
        np.concatenate(
            ([0], filter_ell(np.arange(1, lmax)) / np.arange(1, lmax)**2)))

    filtered_cib = WN + WS
    unfiltered_cib = noise + phi_cross
    if not os.path.exists(folder + '/patches_p_m_{}_std/'.format(fact)):
        os.makedirs(folder + '/patches_p_m_{}_std/'.format(fact))
    overwrite = True
    hp.write_alm(
        folder + '/patches_p_m_{}_std/'.format(fact) +
        'WSN_alms_r{:04d}_{}.fits'.format(idx, case),
        filtered_cib,
        overwrite=overwrite)
    hp.write_alm(
        folder + '/patches_p_m_{}_std/'.format(fact) +
        'WN_alms_r{:04d}_{}.fits'.format(idx, case),
        WN,
        overwrite=overwrite)
    hp.write_alm(
        folder + '/patches_p_m_{}_std/'.format(fact) +
        'WS_alms_r{:04d}_{}.fits'.format(idx, case),
        WS,
        overwrite=overwrite)
    hp.write_alm(
        folder + '/patches_p_m_{}_std/'.format(fact) +
        'S_alms_r{:04d}_{}.fits'.format(idx, case),
        phi_cross,
        overwrite=overwrite)
    hp.write_alm(
        folder + '/patches_p_m_{}_std/'.format(fact) +
        'SN_alms_r{:04d}_{}.fits'.format(idx, case),
        unfiltered_cib,
        overwrite=overwrite)


if __name__ == "__main__":

    # here is where sims live
    folder = './cib_sims/'

    # Load the lensing potential power spectrum used to generate the lensig potential skies
    data = np.loadtxt(
        '/n/holylfs02/LABS/kovac_lab/general/input_maps/official_cl/camb_planck2013_extended_r0_lenspotentialcls.dat'
    )

    folder_phi = '/n/holylfs02/LABS/kovac_lab/general/input_maps/lensing_potentials/camb_planck2013_r0/'

    if not os.path.exists("./data_cib_sims"):
        os.makedirs("./data_cib_sims")

    # sys.exit()
    lmin_alms = 20
    lmax_alms = 1400
    n_sims = 50
    n_cpu = 1

    li = 1. * np.arange(2, lmax_alms + 200)

    pp_spline = InterpolatedUnivariateSpline(data[:, 0],
                                             data[:, 5] / data[:, 0]**2 /
                                             (data[:, 0] + 1.)**2 * 2. * np.pi)
    pp = pp_spline(li)
    # this is to load using splines
    splines = True

    fact_sigma = 0.5
    # This is because the spline are made with ck i.e. the cross with the planck convergence. While the sims generates phi so we have to correct for that.

    cc_mean, cc_plus, cc_minus, cp_mean, cp_plus, cp_minus, lmax = load_spectra_10_patches(
        li, fact=fact_sigma)

    print('you should trust till ell=',
          np.argwhere((cc_mean) < (cp_plus)**2 / (pp))[0],
          'above this the auto CIB become less than the correlated part.')

    print('the spectra are positive except to:', np.argwhere(cc_minus < 0),
          np.argwhere(cp_minus < 0))

    Parallel(n_jobs=n_cpu)(delayed(simulate_parallel_patches)(
        idx=i,
        cc=cc_mean,
        cp=cp_minus,
        cc_filter=cc_mean,
        cp_filter=cp_mean,
        pp=pp,
        li=li,
        case='cross_minus',
        lmin=lmin_alms,
        lmax=lmax_alms,
        fact=fact_sigma,
        folder_sim_output=folder,
        folder_phi=folder_phi) for i in range(1, n_sims))

    Parallel(n_jobs=n_cpu)(delayed(simulate_parallel_patches)(
        idx=i,
        cc=cc_mean,
        cp=cp_plus,
        cc_filter=cc_mean,
        cp_filter=cp_mean,
        pp=pp,
        li=li,
        case='cross_plus',
        lmin=lmin_alms,
        lmax=lmax_alms,
        fact=fact_sigma,
        folder_sim_output=folder,
        folder_phi=folder_phi) for i in range(1, n_sims))

    Parallel(n_jobs=n_cpu)(delayed(simulate_parallel_patches)(
        idx=i,
        cc=cc_plus,
        cp=cp_mean,
        cc_filter=cc_mean,
        cp_filter=cp_mean,
        pp=pp,
        li=li,
        case='auto_plus',
        lmin=lmin_alms,
        lmax=lmax_alms,
        fact=fact_sigma,
        folder_sim_output=folder,
        folder_phi=folder_phi) for i in range(1, n_sims))

    Parallel(n_jobs=n_cpu)(delayed(simulate_parallel_patches)(
        idx=i,
        cc=cc_minus,
        cp=cp_mean,
        cc_filter=cc_mean,
        cp_filter=cp_mean,
        pp=pp,
        li=li,
        case='auto_minus',
        lmin=lmin_alms,
        lmax=lmax_alms,
        fact=fact_sigma,
        folder_sim_output=folder,
        folder_phi=folder_phi) for i in range(1, n_sims))

    Parallel(n_jobs=n_cpu)(delayed(simulate_parallel_patches)(
        idx=i,
        cc=cc_mean,
        cp=cp_mean,
        cc_filter=cc_mean,
        cp_filter=cp_mean,
        pp=pp,
        li=li,
        case='reference',
        lmin=lmin_alms,
        lmax=lmax_alms,
        fact=fact_sigma,
        folder_sim_output=folder,
        folder_phi=folder_phi) for i in range(1, n_sims))

    sys.exit()

# test sims

folder = '/n/holylfs02/LABS/kovac_lab/general/input_maps/lensing_potentials/camb_planck2013_r0/'
folds = {}
for label in ['S', 'WS', 'WN', 'WSN']:
    if label == 'WSN':
        folds[label] = folder + 'cib_WSN_alms_r'
    else:
        folds[label] = folder + label + '_alms_r'


def load_cib_sims(label, sky, case, sign, idx):

    if label in ['S', 'WS', 'WN', 'WSN']:
        if case is 'reference':
            return hp.read_alm(
                folds[label] + '{:04d}_{}_reference.fits'.format(idx, sky))
        else:
            return hp.read_alm(folds[label] + '{:04d}_{}_{}_{}.fits'.format(
                idx, sky, case, sign))


def load_phi_alm(idx):
    return hp.read_alm(
        '/n/holylfs02/LABS/kovac_lab/general/input_maps/lensing_potentials/camb_planck2013_r0/'
        + 'phi_alms_r{0:04d}.fits'.format(idx)).astype('complex128')


for sky in ['BK', 'full']:
    for idx in np.random.randint(1, 500, 8):
        print(('analyzing=', idx))
        filters = []
        filters.append(
            hp.alm2cl(load_cib_sims('WS', sky, 'reference', 'plus', idx)) /
            hp.alm2cl(load_cib_sims('S', sky, 'reference', 'plus', idx)))
        filters.append(
            hp.alm2cl(load_cib_sims('WS', sky, 'auto', 'plus', idx)) /
            hp.alm2cl(load_cib_sims('S', sky, 'auto', 'plus', idx)))
        filters.append(
            hp.alm2cl(load_cib_sims('WS', sky, 'auto', 'minus', idx)) /
            hp.alm2cl(load_cib_sims('S', sky, 'auto', 'minus', idx)))

        filters.append(
            hp.alm2cl(load_cib_sims('WS', sky, 'cross', 'minus', idx)) /
            hp.alm2cl(load_cib_sims('S', sky, 'cross', 'minus', idx)))

        filters.append(
            hp.alm2cl(load_cib_sims('WS', sky, 'cross', 'plus', idx)) /
            hp.alm2cl(load_cib_sims('S', sky, 'cross', 'plus', idx)))
        for filt1 in filters:
            for filt2 in filters:
                np.testing.assert_allclose(filt1, filt2, rtol=1e-03)

sky = 'BK'
for idx in np.random.randint(1, 500, 8):
    print(('analyzing=', idx))
    ws_eq = []
    ws_eq.append(
        hp.alm2cl(load_cib_sims('WS', sky, 'reference', 'plus', idx))[200:])
    ws_eq.append(
        hp.alm2cl(load_cib_sims('WS', sky, 'auto', 'plus', idx))[200:])
    ws_eq.append(
        hp.alm2cl(load_cib_sims('WS', sky, 'auto', 'minus', idx))[200:])
    for filt1 in ws_eq:
        for filt2 in ws_eq:
            np.testing.assert_allclose(filt1, filt2, rtol=1e-03)
