#!/usr/bin/env python
__author__ = "Alessandro Manzotti"
__credits__ = ["Alessandro Manzotti", "Kimmy Wu"]
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "Alessandro Manzotti"
__email__ = "manzotti.alessandro@gmail.com"
"""
The script below was used to compute the needed quantities like auto spectra and cross correlation for Planck CIB and CMB Lensing. These were then used to produce BKSPT sims.

Note: you need polspice and its python wrapper ispice installed. See here: "http://www2.iap.fr/users/hivon/software/PolSpice/"

You can choose if you just want to compute spectra or also plots with the plotting=True options.

As a bonus if you have cmb_footprint package installed you can have a nice plots of the footprints used.
"""

# After the call with Kimmy Sep 28 2017 we decide to switch to Planck only and produce
# CIB auto and cross on full and BK sky.
# These corresponds to 2 different $\rho_{\ell}$
# Then we fit a model to the computed CIB auto and cross spectra (or we spline them) so that we can start building simulations.
# CIB planck maps at:
#
# https://wiki.cosmos.esa.int/planckpla2015/index.php/CMB_and_astrophysical_component_maps#Thermal_dust_and_CIB_all-sky_maps_from_GNILC_component_separation
#
# These are
#
# N_side= 2048  Units = MJy/sr  545 GHz beam = 5 arcmin CIB amplitude at 545 GHz

#
print("starting")
import sys
# sys.path.insert(1, '/sptpol_root/anaconda/lib/python2.7/site-packages/')
import scipy
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import pylab
# pointing to Spice location
spice_dir = '/n/home04/amanzotti/local/PolSpice_v03-05-01/src'
sys.path.append(spice_dir)
import ispice
reload(ispice)
import astropy.io.fits as pyfits
import sys
import healpy
# import statsmodels.api as sm
import healpy as hp
from scipy.interpolate import InterpolatedUnivariateSpline
import numpy as np
from scipy.interpolate import interp1d
import scipy.interpolate
import os

# Change the following to the locations you want

if not os.path.exists("./spectra"):
    os.makedirs("./spectra")
if not os.path.exists("./figures"):
    os.makedirs("./figures")
if not os.path.exists("./data_cib_sims"):
    os.makedirs("./data_cib_sims")

# Data-wise cib maps and CMB lensing phi maps and realizations are required. Point to them here
output_folder = "./spectra"
output_sim_inputs = "./data_cib_sims"
planck_kappa_folder = "/n/holylfs02/LABS/kovac_lab/bkspt/from_spt/cib_data/"
planck_cib_folder = "/n/holylfs02/LABS/kovac_lab/bkspt/from_spt/cib_data/"
planck_dust_folder = "/n/holylfs02/LABS/kovac_lab/bkspt/from_spt/cib_data/"
bicep_mask_file = '/n/holylfs02/LABS/kovac_lab/bkspt/from_spt/cib_data/bk14_mask_gal_n2048.fits'
spice_dir = '/home/amanzotti/local/PolSpice_v03-05-01/src'
sys.path.append(spice_dir)

os.environ['HEALPIX'] = "/n/home04/amanzotti/local/Healpix_3.40/"
# export HEALPIXDATA=/n/holylfs02/LABS/kovac_lab/bkspt/from_spt/cib_data/
# Choose if you want images/plots

plotting = True
if plotting:
    pylab.rcParams['figure.figsize'] = (15, 8)
    plt.rcParams['image.cmap'] = 'viridis'
    matplotlib.rcParams.update({'font.size': 18})
    pylab.rcParams['figure.figsize'] = (15, 8)
    plt.rcParams['image.cmap'] = 'RdBu'


def bl(fwhm_arcmin, lmax):
    """ returns the map-level transfer function for a symmetric Gaussian beam.
         * fwhm_arcmin      - beam full-width-at-half-maximum (fwhm) in arcmin.
         * lmax             - maximum multipole.
    """
    import numpy as np
    ls = np.arange(0, lmax + 1)
    return np.exp(-(fwhm_arcmin * np.pi / 180. / 60.)**2 / (16. * np.log(2.)) *
                  ls * (ls + 1.))


print("computing ful sky auto and cross CIBxlensing spectra")

# load the Bicep mask that clems provided

bicep_mask = hp.read_map(bicep_mask_file, verbose=False)
# remove nan polspice wants zeros
bicep_mask = np.nan_to_num(bicep_mask)
lensing_mask = hp.read_map(planck_kappa_folder + 'mask.fits', verbose=False)
lensing_map = hp.read_map(
    planck_kappa_folder + 'dat_k_map.fits', verbose=False)

#  Read planck Gnilc map

CIB = hp.read_map(
    planck_cib_folder + 'COM_CompMap_CIB-GNILC-F545_2048_R2.00.fits',
    verbose=False)

np.where(CIB < 0)

mask_CIB = np.ones_like(CIB)
mask_CIB[np.where((np.abs(CIB) < 1e-4))] = 0.
hp.write_map(
    planck_cib_folder + 'CIB-GNILC-F545_MASK_2048.fits',
    mask_CIB,
    coord='G',
    overwrite=True)

# convert save a BICEP masks in N_side=2048 to be used with planck maps that have that size.

# # Compute spectra: CIB planck on Full Sky and BK patch

# ### CIB Planck maps on full sky

# Here we compute the power spectrum of Planck CIB cross lensing both on full sky (lensing mask) and on the GASS region

filename = output_folder + '/cross_cib_planck.cl'

ispice.ispice(
    mapin1=planck_cib_folder + 'COM_CompMap_CIB-GNILC-F545_2048_R2.00.fits',
    weightfile1=planck_cib_folder + 'CIB-GNILC-F545_MASK_2048.fits',
    beam1=5,
    pixelfile="YES",
    mapfile2=planck_kappa_folder + 'dat_k_map.fits',
    weightfile2=planck_kappa_folder + 'mask.fits',
    pixelfile2="YES",
    clout=filename,
    apodizesigma=150,
    subdipole='YES',
    subav="YES",
    thetamax=180,
    nlmax=1500)

p = pyfits.open(filename)
p.verify('fix')
cl_cross_planck = p[1].data['TT']

# Note that now we use the mask with 2048 the same as the map, before 4 sept 2017 we were probably doing it wrong.
filename = output_folder + '/auto_cib_planck.cl'
ispice.ispice(
    mapin1=planck_cib_folder + 'COM_CompMap_CIB-GNILC-F545_2048_R2.00.fits',
    beam1=5,
    pixelfile="YES",
    clout=filename,
    maskfile1=planck_kappa_folder + 'mask.fits',
    weightfile1=planck_cib_folder + 'CIB-GNILC-F545_MASK_2048.fits',
    apodizesigma=150,
    subdipole='YES',
    subav="YES",
    thetamax=180,
    nlmax=1500)

p = pyfits.open(filename)
p.verify('fix')
cl_cib_planck = p[1].data['TT']
# ### CIB Planck maps on BK patch

print("DONE. Now  auto and cross CIBxlensing spectra on BK patch")

filename = output_folder + '/cross_cib_planck_BK.cl'

ispice.ispice(
    mapin1=planck_cib_folder + 'COM_CompMap_CIB-GNILC-F545_2048_R2.00.fits',
    weightfile1=bicep_mask_file,
    beam1=5,
    pixelfile="YES",
    mapfile2=planck_kappa_folder + 'dat_k_map.fits',
    weightfile2=planck_kappa_folder + 'mask.fits',
    pixelfile2="YES",
    clout=filename,
    apodizesigma=20,
    subdipole='YES',
    subav="YES",
    thetamax=35,
    nlmax=1500)

p = pyfits.open(filename)
p.verify('fix')
cl_cross_planck_BK = p[1].data['TT']

# Note that now we use the mask with 2048 the same as the map, before 4 sept 2017 we were probably doing it wrong.
filename = output_folder + '/auto_cib_planck_BK.cl'
out = ispice.ispice(
    mapin1=planck_cib_folder + 'COM_CompMap_CIB-GNILC-F545_2048_R2.00.fits',
    beam1=5,
    pixelfile="YES",
    clout=filename,
    weightfile1=bicep_mask_file,
    apodizesigma=20,
    subdipole='YES',
    subav="YES",
    thetamax=35,
    nlmax=1500)
p = pyfits.open(filename)
p.verify('fix')
cl_cib_planck_BK = p[1].data['TT']

bicep_mask = pyfits.open('polarized_dust_alm_gal.fits')

# Dust planck maps and clean regions.

dust_Q = hp.read_map(
    planck_dust_folder + 'COM_CompMap_DustPol-commander_1024_R2.00.fits',
    field=0,
    verbose=False)
dust_U = hp.read_map(
    planck_dust_folder + 'COM_CompMap_DustPol-commander_1024_R2.00.fits',
    field=1,
    verbose=False)

# Save alms so that you can alter them with alteralm function
# first form I = sqrt(Q^u + U^2)

Intensity = np.sqrt(dust_Q**2 + dust_U**2)
dust_int_alm = hp.map2alm(Intensity)

hp.write_alm('./polarized_dust_alm_gal.fits', dust_int_alm, overwrite = True)

# This nededs to be rotated externally with modifyalm

pol_dust_cel = hp.read_alm(planck_dust_folder + './polarized_dust_alm_cel.fits').astype(
    np.complex128)
plo_dust_cel_map = hp.alm2map(pol_dust_cel, 1024)

print("DONE. Defining patches similar to BK")

t_dust = hp.read_map(
    planck_dust_folder + 'COM_CompMap_ThermalDust-commander_2048_R2.00.fits',
    verbose=False)

# thermal_dust_T_alm = hp.map2alm(t_dust)

# # Study variance on different patches
# ## Define patches
# we define the patches as the one with less dust thermal emission (in temperature)

bicep_mask = hp.read_map(bicep_mask_file, verbose=False)
# goal is to reproduce Bicep size and contamination
# print(
#     np.mean(t_dust[np.where(~np.isnan(bicep_mask))[0]]),
#     len(np.where(~np.isnan(bicep_mask))[0]))

patches = []
wrong_patches = []
for dec in np.linspace(0.4, 1.1, 2):
    print(dec)
    for phi in np.linspace(0, 2 * np.pi, 5):
        vec = hp.ang2vec(dec, phi)
        patch = np.zeros_like(bicep_mask)
        patch[healpy.query_disc(2048, vec, 0.315)] = 1
        ratio = np.mean(t_dust[np.where(patch > 0)[0]]) / np.mean(
            t_dust[np.where(~np.isnan(bicep_mask))[0]])
        if ratio < 2.:
            patches.append(patch)
        else:
            wrong_patches.append(patch)

for dec in np.linspace(-0.4 + np.pi, -1.1 + np.pi, 2):
    for phi in np.linspace(0, 2 * np.pi, 5):
        vec = hp.ang2vec(dec, phi)
        patch = np.zeros_like(bicep_mask)
        patch[healpy.query_disc(2048, vec, 0.315)] = 1
        ratio = np.mean(t_dust[np.where(patch > 0)[0]]) / np.mean(
            t_dust[np.where(~np.isnan(bicep_mask))[0]])
        if ratio < 2.:
            patches.append(patch)
        else:
            wrong_patches.append(patch)

len(wrong_patches)

# test that the search worked fine and patches respects the criteria

print("TEST the foreground level for the choosen patches")
fgnd_level = []
for patch in patches:
    fgnd_level.append(np.mean(t_dust[np.where(patch > 0)[0]]))
    print(
        np.mean(t_dust[np.where(patch > 0)[0]]) / np.mean(
            t_dust[np.where(bicep_mask > 0)[0]]),
        len(np.where(patch > 0)[0]) / np.float(
            len(np.where(~np.isnan(bicep_mask))[0])))

# if plotting:

#     import cmb_footprint.footprint

#     fp = cmb_footprint.footprint.SurveyStack(
#         t_dust,
#         fignum=1,
#         projection='mollweide',
#         coord_bg='G',
#         coord_plot='G',
#         cbar=True,
#         title='Test patches')

#     fp.superimpose_hpxmap(
#         np.nan_to_num(mask_CIB),
#         label='Full_sky lensing ',
#         coord_in='G',
#         cbar=False,
#         color='blue')

#     for i, patch in enumerate(patches):
#         fp.superimpose_hpxmap(
#             np.array(patch) * mask_CIB,
#             label='patch {}'.format(i),
#             coord_in='G',
#             cbar=False)

#     fp.superimpose_hpxmap(
#         np.nan_to_num(bicep_mask),
#         label='BK',
#         coord_in='G',
#         cbar=False,
#         color='green')

#     # plt.show()
#     plt.savefig('./figures/patches_maps.png')

# write the mask of the patches on disk cause this is how polspice like it
for i, patch in enumerate(patches):
    hp.write_map(
        output_folder + '/mask_patch_{}.fits'.format(i),
        patch * mask_CIB,
        overwrite=True)

# ## Compute spectra on patches

# Here we use the patches defined above to compute CIB power spectra and cross in low foregrounds region of the sky.

print("Compute cross and auto with Polspice")
print("")

cl_cross_planck_patches = []
cl_cib_planck_patches = []

for i in np.arange(0, 10):
    filename = output_folder + '/cross_cib_patch{}.cl'.format(i)

    ispice.ispice(
        mapin1=planck_cib_folder +
        'COM_CompMap_CIB-GNILC-F545_2048_R2.00.fits',
        weightfile1=output_folder + '/mask_patch_{}.fits'.format(i),
        beam1=5,
        pixelfile="YES",
        mapfile2=planck_kappa_folder + 'dat_k_map.fits',
        weightfile2=planck_kappa_folder + 'mask.fits',
        pixelfile2="YES",
        clout=filename,
        apodizesigma=15,
        subdipole='YES',
        subav="YES",
        thetamax=20,
        nlmax=1500)
    p = pyfits.open(filename)
    p.verify('fix')
    cl_cross_planck_patches.append(p[1].data['TT'])

    # Note that now we use the mask with 2048 the same as the map, before 4 sept 2017 we were probably doing it wrong.
    filename = output_folder + '/auto_cib_planck_patch{}.cl'.format(i)
    out = ispice.ispice(
        mapin1=planck_cib_folder +
        'COM_CompMap_CIB-GNILC-F545_2048_R2.00.fits',
        beam1=5,
        pixelfile="YES",
        clout=filename,
        weightfile1=output_folder + '/mask_patch_{}.fits'.format(i),
        apodizesigma=15,
        subdipole='YES',
        subav="YES",
        thetamax=20,
        nlmax=1500)

    p = pyfits.open(filename)
    p.verify('fix')
    cl_cib_planck_patches.append(p[1].data['TT'])

# BIN DATA IF YOU NEED TO USE THE BIN VERSION
#


def plot_data(data, label='', nbins=12, lmax=1300, plot_data=False,
              color=None):

    # plt.plot(par.li,par.cp/np.sqrt(par.cc*par.pp))
    n, bins = np.histogram(np.arange(0, len(data)), bins=nbins)

    sy, _ = np.histogram(np.arange(0, len(data)), bins=bins, weights=data)

    sy2, _ = np.histogram(
        np.arange(0, len(data)), bins=bins, weights=data * data)
    mean = sy / n
    std = np.sqrt(sy2 / n - mean * mean)
    plt.errorbar(
        (_[1:] + _[:-1]) / 2,
        mean,
        yerr=std,
        fmt='d-',
        label=label,
        color=color)
    if plot_data:
        plt.plot(data, alpha=0.4)
    plt.xlabel(r'$\ell$')


def bin_data(data, nbins=12, lmax=1300):

    # plt.plot(par.li,par.cp/np.sqrt(par.cc*par.pp))
    n, bins = np.histogram(np.arange(0, len(data)), bins=nbins)
    sy, _ = np.histogram(np.arange(0, len(data)), bins=bins, weights=data)

    sy2, _ = np.histogram(
        np.arange(0, len(data)), bins=bins, weights=data * data)
    mean = sy / n
    std = np.sqrt(sy2 / n - mean * mean)
    x_bin = (_[1:] + _[:-1]) / 2
    return x_bin, mean, std


print("DONE. just plot spectra if requested.")

nlk = np.loadtxt(planck_kappa_folder + 'nlkk.dat')

if plotting:

    for i, cl_cross_planck_patch in enumerate(cl_cross_planck_patches):
        color = plt.cm.winter(np.float(i) / len(cl_cib_planck_patches))
        plot_data(
            np.arange(0, len(cl_cross_planck_patch)) * cl_cross_planck_patch,
            label='patch {}'.format(i),
            nbins=12,
            lmax=1400,
            plot_data=False,
            color=color,
            alpha=0.6)

    plot_data(
        np.arange(0, len(cl_cross_planck_BK)) * cl_cross_planck_BK,
        label='bicep',
        nbins=12,
        lmax=1400,
        plot_data=False,
        color='black')

    plot_data(
        np.arange(0, len(cl_cross_planck)) * cl_cross_planck,
        label='full',
        nbins=12,
        lmax=1400,
        plot_data=False,
        color='red')

    plt.ylabel(r'$\ell C^{\phi-CIB}$')
    plt.legend(ncol=3)
    plt.xlim(0, 1000)
    plt.ylim(1e-6, 1.4e-5)
    plt.savefig('./figures/patches_cross.png')
    plt.clf()

    for i, cl_cib_planck_patch in enumerate(cl_cib_planck_patches):
        color = plt.cm.winter(np.float(i) / len(cl_cib_planck_patches))

        plot_data(
            np.arange(0, len(cl_cib_planck_patch)) * cl_cib_planck_patch,
            label='patch {}'.format(i),
            nbins=32,
            lmax=1400,
            plot_data=False,
            color=color,
            alpha=0.6)

    plot_data(
        np.arange(0, len(cl_cib_planck_BK)) * cl_cib_planck_BK,
        label='bicep',
        nbins=12,
        lmax=1400,
        plot_data=False,
        color='black')

    plot_data(
        np.arange(0, len(cl_cib_planck)) * cl_cib_planck,
        label='full',
        nbins=12,
        lmax=1400,
        plot_data=False,
        color='red')

    plt.ylabel(r'$\ell C^{CIB-CIB}$')
    plt.legend(ncol=3)
    plt.xlim(0, 1000)
    plt.ylim(3e-6, 1.4e-5)

    plt.savefig('./figures/patches_auto.png')
    plt.clf()
    for i in np.arange(0, len(cl_cib_planck_patches)):
        color = plt.cm.winter(np.float(i) / len(cl_cib_planck_patches))
        plot_data(
            cl_cross_planck_patches[i] / np.sqrt(
                cl_cib_planck_patches[i] * (nlk[:1501, 2] - nlk[:1501, 1])),
            label='patch {}'.format(i),
            nbins=12,
            lmax=1400,
            plot_data=False,
            color=color)
    plt.ylim(-0.2, 2)

    plot_data(
        cl_cross_planck_BK / np.sqrt(cl_cib_planck_BK *
                                     (nlk[:1501, 2] - nlk[:1501, 1])),
        label='bicep',
        nbins=12,
        lmax=1400,
        plot_data=False,
        color='black')

    plot_data(
        cl_cross_planck / np.sqrt(cl_cib_planck *
                                  (nlk[:1501, 2] - nlk[:1501, 1])),
        label='full',
        nbins=12,
        lmax=1400,
        plot_data=False,
        color='red')

    plt.legend(ncol=4)
    # plt.axhline(0.55,color='gray')
    # plt.axhline(0.80,color='gray')
    plt.fill_between(np.arange(0, 1600), 0.55, 0.85, alpha=0.5)
    plt.xlabel(r"$\ell$")

    plt.ylabel(r"$\rho$")
    plt.savefig('./figures/patches_rho.png')
    plt.clf()

# ### Input for sims

# Compute mean and std of patches. Use that to inform sims

cl_cib_mean = np.mean(cl_cib_planck_patches, 0)
cl_cib_std = np.std(cl_cib_planck_patches, 0)

np.save(output_sim_inputs + '/cl_cib_10patches_171215_cov',
        np.cov(np.array(cl_cib_planck_patches), rowvar=False))
np.save(output_sim_inputs + '/cl_cib_10patches_171215_mean', cl_cib_mean)
np.save(output_sim_inputs + '/cl_cib_10patches_171215_plus',
        cl_cib_mean + cl_cib_std)
np.save(output_sim_inputs + '/cl_cib_10patches_171215_minus',
        cl_cib_mean - cl_cib_std)
np.save(output_sim_inputs + '/cl_cib_10patches_171215_std', cl_cib_std)

if plotting:

    plt.fill_between(
        np.arange(0, len(cl_cib_mean)),
        np.arange(0, len(cl_cib_mean)) * (cl_cib_mean + cl_cib_std),
        np.arange(0, len(cl_cib_mean)) * (cl_cib_mean - cl_cib_std),
        label='mean patches',
        alpha=0.6)

    plot_data(
        np.arange(0, len(cl_cib_planck_BK)) * cl_cib_planck_BK,
        label='bicep',
        nbins=12,
        lmax=1400,
        plot_data=False,
        color='black')

    l_bin, mean_patches, _ = bin_data(
        np.arange(0, len(cl_cib_planck_patches[0])) * np.mean(
            cl_cib_planck_patches, 0),
        nbins=12)

    plt.ylabel(r'$\ell C^{CIB-CIB}$')

    [
        plt.plot(
            np.arange(0, len(cl_cib_planck_patches[i])),
            np.arange(0, len(cl_cib_planck_patches[i])) *
            cl_cib_planck_patches[i]) for i in np.arange(0, 10)
    ]
    plt.xlabel(r'$\ell$')
    plt.ylabel(r'$\ell C^{CIB}_{\ell}$')
    plt.plot(l_bin, mean_patches, 'ro', label='mean')
    plt.legend(loc=0)
    plt.savefig('./figures/auto_pm_input.png')
    plt.clf()

cl_cross_mean = np.mean(cl_cross_planck_patches, 0)
cl_cross_std = np.std(cl_cross_planck_patches, 0)

np.save(output_sim_inputs + '/cl_cross_10patches_171215_std', cl_cross_std)
np.save(output_sim_inputs + '/cl_cross_10patches_171215_mean', cl_cross_mean)
np.save(output_sim_inputs + '/cl_cross_10patches_171215_cov',
        np.cov(np.array(cl_cross_planck_patches), rowvar=False))
np.save(output_sim_inputs + '/cl_cross_10patches_171215_plus',
        cl_cross_mean + cl_cross_std)
np.save(output_sim_inputs + '/cl_cross_10patches_171215_minus',
        cl_cross_mean - cl_cross_std)

data = np.hstack((np.array(cl_cib_planck_patches),
                  np.array(cl_cross_planck_patches)))
cov_matrix = np.cov(data, rowvar=False)
mean = np.hstack((np.mean(cl_cib_planck_patches, 0),
                  np.mean(cl_cross_planck_patches, 0)))

np.save(output_sim_inputs + '/10patches_171215_joint_cov', cov_matrix)
np.save(output_sim_inputs + '/10patches_171215_joint_mean', mean)

if plotting:

    plot_data(
        np.arange(0, len(cl_cross_planck_BK)) * cl_cross_planck_BK,
        label='bicep',
        nbins=12,
        lmax=1400,
        plot_data=False,
        color='black')

    [
        plt.plot(
            np.arange(0, len(cl_cross_planck_patches[i])),
            np.arange(0, len(cl_cross_planck_patches[i])) *
            cl_cross_planck_patches[i], '.-') for i in np.arange(0, 10)
    ]

    l_bin, mean_patches, _ = bin_data(
        np.arange(0, len(cl_cross_planck_BK)) * np.mean(
            cl_cross_planck_patches, 0),
        nbins=12)

    plt.fill_between(
        np.arange(0, len(cl_cross_mean)),
        np.arange(0, len(cl_cross_mean)) * (cl_cross_mean + cl_cross_std),
        np.arange(0, len(cl_cross_mean)) * (cl_cross_mean - cl_cross_std),
        label='mean patches',
        alpha=0.6)

    plt.ylabel(r'$\ell C^{CIB-\phi}$')
    plt.plot(l_bin, mean_patches, 'ro')
    plt.xlabel(r'$\ell$')
    plt.ylabel(r'$\ell C^{CIB-\phi}_{\ell}$')

    plt.legend(loc=0)

    plt.savefig('./figures/cross_pm_input.png')
    plt.clf()

    plt.plot(
        np.mean(
            cl_cross_planck_patches / np.sqrt(
                cl_cib_planck_patches * (nlk[:1501, 2] - nlk[:1501, 1])), 0))

    plot_data(
        cl_cross_planck_BK / np.sqrt(cl_cib_planck_BK *
                                     (nlk[:1501, 2] - nlk[:1501, 1])),
        label='bicep',
        nbins=12,
        lmax=1400,
        plot_data=False,
        color='black')

    l_bin, mean_patches, _ = bin_data(
        np.mean(
            cl_cross_planck_patches / np.sqrt(
                cl_cib_planck_patches * (nlk[:1501, 2] - nlk[:1501, 1])), 0),
        nbins=12)

    plt.plot(l_bin, mean_patches, 'ro')
    plt.axhline(0.65, alpha=0.5, color='gray')
    # cl_cross_planck_patches[i] / np.sqrt(cl_cib_planck_patches[i] *
    #                                              (nlk[:1501, 2] - nlk[:1501, 1]))

    plt.legend(loc=0)
    plt.ylabel(r'$\rho_{\ell}$')

    plt.plot(
        np.mean(cl_cib_planck_patches, 0) -
        np.mean(cl_cross_planck_patches, 0)**2 /
        (nlk[:1501, 2] - nlk[:1501, 1]))
