from __future__ import print_function
from setuptools import setup, find_packages
from setuptools.command.test import test as TestCommand
import io
import codecs
import os
import sys

# import your package
# import sandman

here = os.path.abspath(os.path.dirname(__file__))


def read(*filenames, **kwargs):
    encoding = kwargs.get('encoding', 'utf-8')
    sep = kwargs.get('sep', '\n')
    buf = []
    for filename in filenames:
        with io.open(filename, encoding=encoding) as f:
            buf.append(f.read())
    return sep.join(buf)


long_description = read('README.txt', 'CHANGES.txt')


class PyTest(TestCommand):
    def finalize_options(self):
        TestCommand.finalize_options(self)
        self.test_args = []
        self.test_suite = True

    def run_tests(self):
        import pytest
        errcode = pytest.main(self.test_args)
        sys.exit(errcode)


setup(
    name='cib_bkspt_package',
    # version=cib_bkspt_package.__version__,
    # url='http://github.com/jeffknupp/sandman/',
    # license='Apache Software License',
    author='A. Manzotti',
    tests_require=['pytest'],
    install_requires=['Flask>=0.10.1'],
    cmdclass={'test': PyTest},
    author_email='manzotti.alessandro@gmail.com',
    description='BKSPT CIB sims infrastructure',
    long_description=long_description,
    packages=['cib_bkspt_package'],
    include_package_data=True,
    platforms='any',
    test_suite='cib_bkspt_package.test.test_cib_bkspt_package',
    classifiers=[
        'Programming Language :: Python',
        'Natural Language :: English',
        'License :: OSI Approved :: Apache Software License',
        'Operating System :: OS Independent',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
    extras_require={
        'testing': ['pytest'],
    })
